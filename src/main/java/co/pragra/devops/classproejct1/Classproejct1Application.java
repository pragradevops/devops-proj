package co.pragra.devops.classproejct1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Classproejct1Application {

	public static void main(String[] args) {
		SpringApplication.run(Classproejct1Application.class, args);
	}

}
